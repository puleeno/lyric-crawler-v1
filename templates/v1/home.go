package v1

import (
	"gitlab.com/puleeno/lyric-crawler-v1/app/controllers"
)

type HomeController struct{
	controllers.ThemeController
}


func (c *HomeController) Get(){
	c.TplName = c.View("home", "index")
}