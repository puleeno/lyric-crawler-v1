package zingmp3

type Artist struct {
	Artist_Id int
	Name string
	Alias string
	Birthname string
	Birthday string
	Sex int
	Genre_Id string
	Avatar string
	Cover string
	Cover3 string
	Zme_Acc string
	Role string
	Website string
	Biography string
	Agency_Name string
	National_Name string
	Is_Official int
	Year_Active string
	Status_Id int
	Created_Date int
	Link string
	Genre_Name string
	Response Response
}