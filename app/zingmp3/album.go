package zingmp3

type Album struct {
	Playlist_Id string
	Title string
	Artist_Id string
	Artist string
	Genre_Id string
	Zaloid int
	Username string
	Cover string
	Total_Track int
	Description string
	Is_Hit int
	Is_Official int
	Is_Album int
	Year string
	Status_Id int
	Link string
	Total_Play int
	Genre_Name string
	Likes int
	Like_This bool
	Comments int
	Favourites int
	Favourite_This bool
	Response Response
}