package zingmp3


type Song struct {
	Song_Id int
	Song_Id_Encode string
	Video_Id_Encode string
	Title string
	Artist_Id string
	Artist string
	Album_Id int
	Album string
	Composer_Id int
	Composer string
	GenreID string
	Isrc string
	ZaloID int
	UserName string
	IsHit int
	Is_Official int
	Duration int
	DownloadStatus int
	Copyright string
	CoID int
	AdStatus int
	LicenseStatus int
	Lyrics_File string
	DownloadDisable int
	VNOnly bool
	TotalPlay int
	Link string
	Source map[string]string
	LinkDownload map[string]string
	Thumbnail string
	AlbumCover string
	Likes int
	LikeThis bool
	Favourites int
	FavouriteThis bool
	Comments int
	Genre_Name string
	Video *Video
	Response *Response
}