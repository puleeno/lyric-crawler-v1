package run

import (
	"github.com/astaxie/beego/orm"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
)

func Crawler(){
	o := orm.NewOrm()
	var links []*models.CrawlerLink

	qs := o.QueryTable(new(models.CrawlerLink)).Filter("Searched", false).Filter("Updated", false).Limit(vars.CurrentSite.MaxCrawlLink)
	_, err := qs.All(&links)
	if err == nil{
		for _,item := range links{
			vars.CrawlLink(item)
		}
	}
}