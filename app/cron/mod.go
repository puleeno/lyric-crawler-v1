package cron

import (
	"os"
	"time"
	"reflect"
	"strconv"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
	"gitlab.com/puleeno/lyric-crawler-v1/app/cron/run"
	"gitlab.com/puleeno/lyric-crawler-v1/app/cron/update"
	"gitlab.com/puleeno/lyric-crawler-v1/app/cron/flag"
)

func DetectCron(task []string){
	vars.CurrentSite = LoadCurrentSite()
	switch task[1] {
	case "run":
		switch task[2]{
		case "crawler":
			f, err := os.OpenFile( vars.Cmd + "test.txt", os.O_APPEND|os.O_WRONLY, 0600)
			if err != nil {
				panic(err)
			}

			defer f.Close()

			if _, err = f.WriteString(time.Now().String() + "\n"); err != nil {
				panic(err)
			}
			run.Crawler()
		case "search":
			run.Search()
		}

	case "update":
		switch task[2] {
		case "link":
			update.Keywords();
		case "keyword":
			update.Links();
		}

	case "flag":
		switch task[2] {
		case "link":
			if(len(task) > 3){
				switch task[3] {
				case "priority1":
					flag.LinkPriority1()
				case "priority2":
					flag.LinkPriority2()
				case "priority3":
					flag.LinkPriority3()
				case "priority4":
					flag.LinkPriority4()
				}
			}
		case "keyword":
			flag.Keywords()
		}
	}
}

func LoadCurrentSite() *vars.Site{
	site := &vars.Site{}
	var configs []*models.Config
	qs := vars.Orm.QueryTable(new(models.Config))
	qs.All(&configs)

	v := reflect.ValueOf(site).Elem()

	t := v.Type()

	for _, config := range configs{
		for i:= 0;i<t.NumField();i++{
			if t.Field(i).Name == config.Name{
				switch config.Type{
				case "time":
					t, err := time.Parse("2006-01-02 15:04:05 -0700 ICT", config.Value)
					if err == nil{
						v.Field(i).Set(reflect.ValueOf(t))
					}
				case "int":
					num, err := strconv.ParseInt(config.Value, 10, 64)
					if err != nil{
						num = 0
					}
					v.Field(i).Set(reflect.ValueOf(int(num)))
				default:
					v.Field(i).Set(reflect.ValueOf(config.Value))
				}
				continue
			}
		}
	}

	return site
}


