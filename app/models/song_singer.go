package models

type SongSinger struct{
	Id uint
	Song *Song `orm:"rel(fk)"`
	Singer *Person `orm:"rel(fk)"`
}
