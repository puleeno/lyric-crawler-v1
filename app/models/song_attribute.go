package models

type SongAttribute struct {
	Id uint
	Attribute *Attribute `orm:"rel(fk)"`
	Song *Song `orm:"rel(fk)"`
	Value string `orm:"null;type(text)"`
}
