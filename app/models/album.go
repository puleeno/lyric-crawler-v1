package models

type Album struct {
	Id uint
	Name string
	Slug string
	CoverImage string `orm:"null"`
	PublishedAt string `orm:"null;type(varchar)"`
	Description string `orm:"type(text);null;"`
	SeoTitle string `orm:"null"`
	SeoDescription string `orm:"null"`
	SeoKeywords string `orm:"null"`
	Songs []*Song `orm:"reverse(many)"`
	Singers []*Person `orm:"rel(m2m);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SingerAlbum)"`
}

func (a *Album) TableName() string{
	return "albums"
}