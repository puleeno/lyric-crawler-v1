package models

import "time"

type CrawlerLink struct{
	Id uint
	Link string
	Searched bool `orm:"default(false)"`
	Updated bool `orm:"default(false)"`
	LastSeach time.Time `orm:"type(timestamp);auto_now"`
	Priority int `orm:"default(4)"`
}

func (cl *CrawlerLink) TableName() string{
	return "crawler_links"
}