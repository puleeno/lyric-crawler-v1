package models

type Link struct {
	Id uint
	AdsLink string `orm:"null"`
	RegularLink string `orm:"null"`
	MediaId string `orm:"null"`
	Host *Host `orm:"rel(fk)"`
	Song *Song `orm:"rel(fk)"`
}

func(l *Link) TableName() string{
	return "links"
}