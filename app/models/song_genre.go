package models

type SongGenre struct{
	Id uint
	Song *Song `orm:"rel(fk)"`
	Genre *Genre `orm:"rel(fk)"`
}
