package models

type Attribute struct{
	Id uint
	Name string
	Slug string `orm:"size(100)"`
	Description string `orm:"null"`
	Songs []*Song `orm:"reverse(many)"`
}

func (a *Attribute) TableName() string{
	return "attributes"
}