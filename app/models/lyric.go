package models


type Lyric struct{
	Id uint
	Content string `orm:"type(text)"`
	Song *Song `orm:"rel(fk)"`
}

func (l *Lyric) TableName() string{
	return "lyrics"
}