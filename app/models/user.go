package models

import (
	"time"
	"github.com/astaxie/beego/orm"
)

type User struct{
	Id uint
	Name string `orm:"null"`
	Username string `orm:"null;type(varchar);size(100);unique"`
	Email string `orm:"type(varchar);size(100);unique"`
	Password string `orm:"type(char);size(100)"`
	RememberToken string `orm:"null"`
	Status bool `orm:"default(false)"`
	CreatedAt time.Time `orm:"auto_now_add;type(datetime)"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)"`
}

func (m *User) Read(fields ...string) error{
	if err := orm.NewOrm().Read(m, fields...); err != nil{
		return err
	}
	return nil
}


func(u *User) TableName() string{
	return "users"
}