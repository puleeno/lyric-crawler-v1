package models

import "time"

type Keyword struct{
	Id uint
	Keyword string
	Searched bool `orm:"default(false)"`
	Updated bool `orm:"default(false)"`
	LastSeach time.Time `orm:"type(timestamp)"`
}

func (k *Keyword) TableName() string{
	return "keywords"
}
