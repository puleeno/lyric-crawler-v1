package models

type Genre struct {
	Id uint
	Name string
	Slug string
	Description string `orm:"type(text);null"`
	SeoTitle string `orm:"null"`
	SeoDescription string `orm:"null"`
	SeoKeywords string `orm:"null"`
	Songs []*Song `orm:"reverse(many);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongGenre)"`
}

func (g *Genre) TableName() string{
	return "genres"
}
