package models

type Song struct {
	Id uint
	Name string
	Slug string
	OtherName string `orm:"null"`
	Thumbnail string `orm:"null"`
	SeoTitle string `orm:"null"`
	SeoDescription string `orm:"null"`
	SeoKeywords string `orm:"null"`
	Writer []*Person `orm:"rel(m2m);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongWriter)"`
	Singer []*Person `orm:"rel(m2m);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongSinger)"`
	Genres []*Genre `orm:"null;rel(m2m);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongGenre)"`
	Album *Album `orm:"null;rel(fk)"`
	Attributes []*Attribute `orm:"rel(m2m);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongAttribute)"`
	Lyrics []*Lyric `orm:"reverse(many)"`
	Links []*Link `orm:"reverse(many)"`
}

func (s *Song) TableName() string{
	return "songs"
}
