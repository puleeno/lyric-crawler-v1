package models

type SongWriter struct {
	Id uint
	Song *Song `orm:"rel(fk)"`
	Writer *Person `orm:"rel(fk)"`
}
