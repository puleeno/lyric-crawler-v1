package models

type Config struct {
	Id uint
	Name string `orm:"type(varchar);size(100)"`
	Type string `orm:"type(varchar);size(100)"`
	Value string `orm:";null"`
}

func (c *Config) TableName() string{
	return "configs"
}