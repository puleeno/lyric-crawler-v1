package models

type Person struct{
	Id uint
	Name string
	Slug string
	RealName string `orm:"null"`
	ProfileImage string `orm:"null"`
	Bio string `orm:"null;type(text)"`
	Birdthday string `orm:"null;type(date)"`
	Gender int `orm:"null"`
	SeoTitle string `orm:"null"`
	SeoDescription string `orm:"null"`
	SeoKeywords string `orm:"null"`
	Writed []*Song `orm:"reverse(many);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongWriter)"`
	Singed []*Song `orm:"reverse(many);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SongSinger)"`
	Albums []*Album `orm:"null;reverse(many);rel_through(gitlab.com/puleeno/lyric-crawler-v1/app/models.SingerAlbum)"`
}

func (p *Person) TableName() string{
	return "persons"
}