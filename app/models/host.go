package models

type Host struct{
	Id uint
	Name string `orm:"null"`
	Domain string `orm:"unique"`
	StaticUrl string `orm:"null;unique"`
	Description string `orm:"null"`
	Icon string `orm:"null"`
	Link []*Link `orm:"reverse(many)"`
}

func (h *Host) TableName() string{
	return "hosts"
}