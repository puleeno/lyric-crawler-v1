package models

type SingerAlbum struct{
	Id uint
	Singer *Person `orm:"rel(fk)"`
	Album *Album `orm:"rel(fk)"`
}
