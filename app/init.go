package app

import (
	"time"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego/orm"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
	"github.com/astaxie/beego"
	"gitlab.com/puleeno/lyric-crawler-v1/app/controllers"
	"gitlab.com/puleeno/lyric-crawler-v1/templates/v1"
	_"gitlab.com/puleeno/lyric-crawler-v1/app/helpers"
)

func init(){
	RegisterRouter();
	RegisterAdminRouter()
	InitDB()
	beego.SetStaticPath("/app/", vars.Cmd + "/app/assets")
	beego.SetStaticPath("/app/", vars.Cmd + "/templates/" + vars.CurrentTheme + "/assets")
}

func InitDB(){
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", vars.ConnectionString)

	orm.RegisterModelWithPrefix(vars.DbPrefix,
		new(models.User),
		new(models.Config),
		new(models.CrawlerLink),
		new(models.Keyword),

		new(models.Genre),
		new(models.Attribute),
		new(models.Song),
		new(models.Lyric),
		new(models.Link),
		new(models.Host),
		new(models.Person),
		new(models.Album),

		new(models.SingerAlbum),
		new(models.SongAttribute),
		new(models.SongGenre),
		new(models.SongSinger),
		new(models.SongWriter),
	)

	vars.Orm = orm.NewOrm()

	if(!CheckInstalled()){
		orm.RunSyncdb("default", true, true)
		SeedDB();
	}

	vars.DomainsAllow = vars.GetDomainAllow()
}

func RegisterRouter(){
	beego.Router("/", &v1.HomeController{})
}

func RegisterAdminRouter(){
	ns := beego.NewNamespace("/admin",
		beego.NSRouter("/", &controllers.DashboardController{}),
		beego.NSNamespace("/user",
			beego.NSRouter("/", &controllers.UserController{}),
		),
	)
	beego.Router(vars.LoginURL, &controllers.AuthController{}, "*:DoLogin")
	beego.AddNamespace(ns)
}

func SeedDB(){

	c := &models.Config{
		Name: "InstalledAt",
		Type: "time",
		Value: time.Now().String(),
	}
	vars.Orm.Insert(c)

	c = &models.Config{
		Name: "MaxCrawlLink",
		Type: "int",
		Value: "20",
	}
	vars.Orm.Insert(c)

	c = &models.Config{
		Name: "Name",
		Type: "text",
		Value: "Puleeno Nguyen Blog",
	}

	vars.Orm.Insert(c)

	c = &models.Config{
		Name: "domain_allow",
		Type: "text",
		Value: "mp3.zing.vn",
	}

	vars.Orm.Insert(c)

	u := &models.User{
		Name: "Puleeno Nguyen",
		Username: "puleeno",
		Email: "puleeno@gmail.com",
		Password: "$2a$10$Cm6zb/.fC0c.vCLg/Mzqz.TS6neTX3ay6xj9D3E8gqJqoAhnnW/Du",
	}
	vars.Orm.Insert(u)

	l := &models.CrawlerLink{
		Link: "http://mp3.zing.vn/bai-hat/Khi-Anh-Khong-Con-Yeu-Nguyen-Dinh-Vu/ZW6A8UA7.html",
		LastSeach: time.Now(),
	}
	vars.Orm.Insert(l)
}

func CheckInstalled() bool{
	i := &models.Config{
		Name: "InstalledAt",
	}
	if err := vars.Orm.Read(i, "Name"); err == nil{
		return true
	}
	return false
}