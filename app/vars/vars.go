package vars

import (
	"os"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"net/http"
	"time"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
)

var(
	AdminMenu []*MenuItem
	CurrentTheme string
	SEO *Meta
	ConnectionString string
	DbPrefix string
	Orm orm.Ormer
	CurrentSite *Site
	LoginURL string
	RegisterURL string
	AppKey string
	Cmd string
	DomainsAllow []string

	myClient = &http.Client{
		Timeout: 10 * time.Second,

	}
)

func init(){
	if(os.Getenv("OPENSHIFT_MYSQL_DB_HOST") == ""){
		os.Setenv("OPENSHIFT_MYSQL_DB_HOST", beego.AppConfig.String("dbhost"))
	}
	if(os.Getenv("OPENSHIFT_MYSQL_DB_PORT") == ""){
		os.Setenv("OPENSHIFT_MYSQL_DB_PORT", beego.AppConfig.String("dbport"))
	}
	if(os.Getenv("OPENSHIFT_MYSQL_DB_USERNAME") == ""){
		os.Setenv("OPENSHIFT_MYSQL_DB_USERNAME", beego.AppConfig.String("dbuser"))
	}
	if(os.Getenv("OPENSHIFT_MYSQL_DB_PASSWORD") == ""){
		os.Setenv("OPENSHIFT_MYSQL_DB_PASSWORD", beego.AppConfig.String("dbpass"))
	}
	if(os.Getenv("OPENSHIFT_APP_NAME") == ""){
		os.Setenv("OPENSHIFT_APP_NAME", beego.AppConfig.String("dbname"))
	}

	ConnectionString = os.Getenv("OPENSHIFT_MYSQL_DB_USERNAME")
	ConnectionString += ":"
	ConnectionString += os.Getenv("OPENSHIFT_MYSQL_DB_PASSWORD")
	ConnectionString += "@tcp(";
	ConnectionString += os.Getenv("OPENSHIFT_MYSQL_DB_HOST")
	ConnectionString += ":";
	ConnectionString += os.Getenv("OPENSHIFT_MYSQL_DB_PORT")
	ConnectionString += ")/";
	ConnectionString += os.Getenv("OPENSHIFT_APP_NAME")
	ConnectionString += "?charset="
	ConnectionString += beego.AppConfig.String("charset")



	AdminMenu = NewAdminMenu()
	CurrentTheme = beego.AppConfig.String("theme")
	SEO = &Meta{}
	DbPrefix = beego.AppConfig.String("dbprefix")

	AppKey = beego.AppConfig.String("appkey")
	LoginURL = GetLoginUrl()
	RegisterURL = GetRegisterUrl()
	Cmd = GetCmdPath()
}


func NewAdminMenu() []*MenuItem{
	m  := []*MenuItem{
		&MenuItem{
			Label: "Quản lý",
			URL: "/admin",
		},
		&MenuItem{
			Label: "Bài viết",
			URL: "/admin/post",
		},
		&MenuItem{
			Label: "Thành viên",
			URL: "/admin/user",
		},
	}
	return m
}

func GetLoginUrl() string{
	login_url := beego.AppConfig.String("login_url")
	if login_url == ""{
		login_url = "/auth/login"
	}
	return login_url
}

func GetRegisterUrl() string{
	register_url := beego.AppConfig.String("register_url")
	if register_url == ""{
		register_url = "/auth/register"
	}
	return register_url
}

func GetCmdPath() string{
	path := os.Getenv("OPENSHIFT_REPO_DIR")
	if path == ""{
		path = os.Getenv("GOPATH") + "/src/" + beego.AppConfig.String("cmddir")
	}
	return path
}

func GetDomainAllow() []string{
	result := []string{}
	o := orm.NewOrm()
	var domains []*models.Config
	qs := o.QueryTable(new(models.Config)).Filter("Name", "domain_allow")
	qs.All(&domains)

	for _,domain := range domains{
		result = append(DomainsAllow, domain.Value)
	}
	return result
}