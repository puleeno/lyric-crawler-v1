package vars

import "time"

type Site struct{
	Name string
	Domain string
	Slogan string
	Description string
	Keywords string
	InstalledAt time.Time
	SeoTitle string
	MaxCrawlLink int
	SeoDescription string
	SeoKeywords string
	SeoImage string
	RobotIndex string
}