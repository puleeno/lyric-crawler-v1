package vars

import (
	"fmt"
	"time"
	"bytes"
	"strings"
	"net/url"
	"github.com/PuerkitoBio/goquery"
	"github.com/jackdanger/collectlinks"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
	"gitlab.com/puleeno/lyric-crawler-v1/app/zingmp3"
	"github.com/gosimple/slug"
)


func CrawlLink(link *models.CrawlerLink){
	url, _ := url.Parse(link.Link)

	if url, err := url.Parse(link.Link); err == nil && CheckDomainAllowed(url.Host){
		if doc, err := goquery.NewDocument(link.Link); err == nil{
			switch{
				case url.Host == "mp3.zing.vn" && strings.HasPrefix(url.RequestURI(), "/bai-hat/"):
					CrawlSong(doc,url)
				case url.Host == "mp3.zing.vn" && strings.HasPrefix(url.RequestURI(), "/album/"):
					CrawlAlbum(doc, url)
				case url.Host == "mp3.zing.vn" && strings.HasPrefix(url.RequestURI(), "/nghe-si/"):
					CrawlPerson(doc, url)
			}

			html,_ := doc.Html()
			links := collectlinks.All(bytes.NewBufferString(html));
			for _,link := range links{
				u,err := url.Parse(link)
				if err == nil && (CheckDomainAllowed(u.Host) || u.Host == ""){
					full_link := u.Scheme + "://" + u.Host + u.RequestURI()
					l := &models.CrawlerLink{
						Link: full_link,
					}
					switch{
						case u.RequestURI() == "/":
						l.Priority = 1
						case strings.HasPrefix(u.RequestURI(), "/nghe-si/"):
						l.Priority = 3
						case strings.HasPrefix(url.RequestURI(), "/bang-xep-hang/"):
						l.Priority = 2
						case strings.HasPrefix(u.RequestURI(), "/the-loai-video/"):
						l.Priority = 2
						default:
						l.Priority = 4
					}
					if err := Orm.Read(l, "Link"); err != nil{
						Orm.Insert(l)
					}
				}
			}
		}
		link.Searched = true
		link.LastSeach = time.Now()
		Orm.Update(link)
	}
}

func CrawlSong(doc *goquery.Document, url *url.URL) *models.Song{
	var result *models.Song
	switch {
	case url.Host == "mp3.zing.vn":
		media_id, exists := doc.Find("#mp3-common-info").Attr("data-mediaid")
		if exists{
			result = CrawlSongFromMediaID(media_id, url)

			if result != nil{
				lyric := &models.Lyric{
					Song: result,
				}
				if err := Orm.Read(lyric, "Song"); err != nil{
					if html, err := doc.Find(".fn-wlyrics.fn-content").Html(); err == nil{
						lyric.Content = html
						Orm.Insert(lyric)
					}
				}
			}
		}
	}
	return result
}
func CrawlAlbum(doc *goquery.Document, url *url.URL){
	switch {
	case url.Host == "mp3.zing.vn":
		media_id, exists := doc.Find("#playlistItems li").Attr("data-id")
		if exists{
			CrawlAlbumFromSongID(media_id, url)
		}
	}
}
func CrawlPerson(doc *goquery.Document, url *url.URL){
	switch {
	case url.Host == "mp3.zing.vn":
		media_id, exists := doc.Find(".list-item.full-width li").Attr("data-id")
		if exists{
			CrawlPersonFromSongID(media_id, url)
		}
	}
}

func CrawlGenre(html string){}


func CrawlSongFromMediaID(media_id string, url *url.URL) *models.Song{
	var result *models.Song
	song := &zingmp3.Song{}
	song_url := fmt.Sprintf(zingmp3.LinkCrawlSong, media_id)
	if err := GetFromURL(song_url, song); err == nil && song.Is_Official == 1{
		generes := QueryGenre(strings.Split(song.Genre_Name, ","))
		composer := QueryPerson(strings.Split(song.Composer, "ft."))
		singers := QueryPerson(strings.Split(song.Artist, "ft."))
		album := CrawlAlbumFromID(song.Album_Id, url)
		songdb := QuerySong(song.Title, composer,singers,generes,album)
		result = songdb

		h := &models.Host{
			Domain: url.Host,
		}
		if err := Orm.Read(h, "Domain"); err != nil{
			Orm.Insert(h)
		}
		link := &models.Link{
			Host: h,
			Song: songdb,
			MediaId: song.Song_Id_Encode,
			RegularLink: song.Link,
		}
		if err := Orm.Read(link, "Host", "Song", "MediaID", "RegularLink"); err != nil{
			Orm.Insert(link)
		}

	}else{
		fmt.Println(err)
	}
	return result
}

func CrawlAlbumFromID(album_id int, url *url.URL) *models.Album{
	var result *models.Album
	switch{
	case url.Host == "mp3.zing.vn":
		album := &zingmp3.Album{}
		album_url := fmt.Sprintf(zingmp3.LinkCrawlAlbum, album_id)
		if err := GetFromURL(album_url, album) ; err == nil && album.Is_Official == 1{
			persons := QueryPerson(strings.Split(album.Artist, ","))

			result = QueryAlbum(album.Title, persons)
			result.CoverImage = album.Cover
			result.Description = album.Description
			result.PublishedAt = album.Year
			result.Singers = persons

			Orm.Update(result)


			if result.Singers != nil{
				Orm.QueryM2M(result, "Singers").Add(result.Singers)
			}
		}
	}
	return result
}

func CrawlAlbumFromSongID(media_id string, url *url.URL) *models.Album{
	var result *models.Album
	switch{
	case url.Host == "mp3.zing.vn":
		song := &zingmp3.Song{}
		song_url := fmt.Sprintf(zingmp3.LinkCrawlSong, media_id)
		if err := GetFromURL(song_url, song); err == nil && song.Is_Official == 1{
			CrawlAlbumFromID(song.Album_Id, url)
		}
	}
	return result
}

func CrawlPersonFromSongID(media_id string, url *url.URL) *models.Album{
	var result *models.Album
	switch{
	case url.Host == "mp3.zing.vn":
		song := &zingmp3.Song{}

		song_url := fmt.Sprintf(zingmp3.LinkCrawlSong, media_id)
		if err := GetFromURL(song_url, song); err == nil && song.Is_Official == 1{
			CrawlPersonFromID(strings.Split(song.Artist_Id, ","), url)
		}
	}
	return result
}

func CrawlPersonFromID(person_ids []string, url *url.URL) []*models.Person{
	result := make([]*models.Person, len(person_ids))

	switch{
	case url.Host == "mp3.zing.vn":
		for i,person_id := range person_ids{
			person := &zingmp3.Artist{}
			person_url := fmt.Sprintf(zingmp3.LinkCrawlArtist, person_id)
			if err := GetFromURL(person_url, person) ; err == nil && person.Is_Official == 1{
				persion_obj := &models.Person{
					Name: strings.Trim(person.Name, " "),
				}
				if err := Orm.Read(persion_obj, "Name") ; err != nil{
					Orm.Insert(persion_obj)
				}
				persion_obj.Bio = person.Biography
				persion_obj.Birdthday = person.Birthday
				persion_obj.RealName = person.Birthname
				persion_obj.ProfileImage = person.Avatar
				Orm.Update(persion_obj)
				result[i] = persion_obj
			}
		}
	}
	return result
}


func QuerySong(name string, composers []*models.Person, singers []*models.Person, generes []*models.Genre, album *models.Album) *models.Song{
	song := &models.Song{
		Name: strings.Trim(name, " "),
		Writer: composers,
		Singer: singers,
		Album: album,
		Genres: generes,
	}
	if err := Orm.Read(song, "Name", "Writer", "Singer", "Album", "Genres") ; err != nil{
		song.Slug = slug.Make(song.Name)
		Orm.Insert(song)
	}

	if song.Genres != nil{
		Orm.QueryM2M(song, "Genres").Add(song.Genres)
	}

	if song.Singer != nil{
		Orm.QueryM2M(song, "Singer").Add(song.Singer)
	}

	if song.Writer != nil {
		Orm.QueryM2M(song, "Writer").Add(song.Writer)
	}

	return song
}

func QueryAlbum(name string, singers []*models.Person) *models.Album{
	alb := &models.Album{
		Name: name,
		Singers: singers,
	}
	if err := Orm.Read(alb, "Name", "Singers"); err != nil{
		alb.Slug = slug.Make(alb.Name)
		Orm.Insert(alb)
	}
	return alb
}

func QueryGenre(names []string) []*models.Genre{
	genres := make([]*models.Genre, len(names))
	for i,name := range names{
		gen := &models.Genre{
			Name: strings.Trim(name, " "),
		}
		if err := Orm.Read(gen, "Name"); err != nil{
			gen.Slug = slug.Make(gen.Name)
			Orm.Insert(gen)
		}
		genres[i] = gen
	}
	return genres
}

func QueryPerson(names []string) []*models.Person{
	persons := make([]*models.Person, len(names))
	for i, name := range names{
		person := &models.Person{
			Name: strings.Trim(name, " "),
		}

		if err := Orm.Read(person, "Name"); err != nil{
			person.Slug = slug.Make(person.Name)
			Orm.Insert(person)
		}
		persons[i] = person
	}
	return persons
}
