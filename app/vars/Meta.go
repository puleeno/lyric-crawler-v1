package vars

type Meta struct {
	Title string
	Description string
	Keywords string
	Robot string
	Author string
	Me string
	Publisher string
	ogTitle string
	ogType string
	ogImage string
	ogURL string
	ogDescription string
	ogAdmin string
	TwitterCard	string
	TwitterURL string
	TwitterTitle string
	TwitterDescription string
	TwitterImage string
}