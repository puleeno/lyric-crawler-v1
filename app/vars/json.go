package vars

import "encoding/json"

func GetFromURL(url string, model interface{}) error{
	r, err := myClient.Get(url)

	if err != nil{
		return err
	}

	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(model)
}
