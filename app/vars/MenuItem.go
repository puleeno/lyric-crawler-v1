package vars

type MenuItem struct{
	Label string
	URL string
	Icon string
	Before string
	After string
	BeforeLink string
	AfterLink string
	Submenu []*MenuItem
}