package helpers

import (
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
	"github.com/kennygrant/sanitize"
)

func SiteSeoDescription(s *vars.Site) string{
	var result string
	if s.SeoDescription != ""{
		result = s.SeoDescription
	}else{
		result = s.Description
		if result == ""{
			result = "Mã nguồn được xây dựng bởi 👨🏻 Puleeno Nguyen (💌 puleeno@gmail.com) điện thoại ☎️ 0981272899"
		}
	}
	result = sanitize.HTML(result)
	return result
}

func SiteSeoTitle(s *vars.Site) string{
	title := s.SeoTitle
	if title == ""{
		title = s.Slogan
		if(title == ""){
			title = "Thiết kế web chuyên nghiệp 💵 chuẩn SEO"
		}
	}
	result := title + " | " + vars.CurrentSite.Name
	if len(result) > 60{
		result = title
	}
	result = sanitize.HTML(result)
	return result
}