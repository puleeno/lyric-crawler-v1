package helpers

import (
	"fmt"
	"html/template"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
)

func SEO() template.HTML{
	result := ""
	if vars.SEO.Title != ""{
		result += fmt.Sprintf("<title>%s</title>\n", vars.SEO.Title)
	}
	if(vars.SEO.Description != ""){
		result += fmt.Sprintf("\t<meta name=\"description\" value=\"%s\">", vars.SEO.Description)
	}
	result += Developer()
	return template.HTML(result)
}

func BuildMeta(obj interface{}){
	seo := vars.SEO
	if s,ok := obj.(*vars.Site); ok{
		seo.Title = SiteSeoTitle(s)
		seo.Description = SiteSeoDescription(s)
	}
}

func Developer() string{
	return `
	<!--
	Developer: Puleeno Nguyen
	Contact: 0981 272 899
	Email: puleeno@gmail.com
	-->
	`
}