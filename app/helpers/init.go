package helpers

import "github.com/astaxie/beego"

func init(){
	beego.AddFuncMap("SEO", SEO)
	beego.AddFuncMap("body_class", BodyClass)
	beego.AddFuncMap("site_logo", SiteLogoHTML)
}
