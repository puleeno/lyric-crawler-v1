package helpers

import (
	"fmt"
	"html/template"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
)

func BodyClass(classes []string) string{
	var result string
	for i,class := range classes{
		if(i != 0){
			result += " "
		}
		result += class
	}
	return result
}

func SiteLogoHTML(page string) template.HTML{
	var html string
	var tag string

	if page == "home"{
		tag = "h1"
	}else{
		tag = "div"
	}
	html = fmt.Sprintf(`<%[1]s class="site-logo" id="brand"><a href="%[2]s" title="%[3]s">%[3]s</a></%[1]s>`, tag, "/", vars.CurrentSite.Name)
	return template.HTML(html)
}
