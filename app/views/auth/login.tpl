<h1>{{ .Title }}</h1>
<form action="{{ urlfor "AuthController.DoLogin" }}" method="post">
	<div>
		<div class="username--Input">
			<input type="text" name="username">
		</div>
		<div class="password-Input">
			<input type="password" name="password">
		</div>
		<div class="remember--Me">
			<input type="checkbox" id="remember" name="remember" value="true"> <label for="remember">Ghi nhớ tài khoản</label>
		</div>
		<div class="form--Submit">
			<button class="btn btn--Submit">Đăng nhập</button>
		</div>
	</div>
	<input type="hidden" name="redirect" value="{{ .Referer }}">
</form>