<ul class="sidebar-menu">
    {{ range .AdminMenu }}
    <li>
        <a href="{{ .URL }}" title="{{ .Label }}">{{ .Label }}</a>
    </li>
    {{ end }}
</ul>