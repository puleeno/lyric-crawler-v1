package controllers

type UserController struct{
	AdminController
}

func (c *UserController) Get(){
	c.TplName = c.View("user", "index")
}
