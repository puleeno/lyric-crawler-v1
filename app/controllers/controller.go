package controllers

import (
	"time"
	"reflect"
	"strconv"
	"github.com/astaxie/beego"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
)

type BaseController struct {
	beego.Controller
	IsLogin bool
	CurrentUser *models.User
	BodyClass []string
}

func (c *BaseController) Prepare(){
	c.LayoutSections = make(map[string]string)
	user, loggedin := c.CheckLogin()
	c.IsLogin = loggedin
	if loggedin{
		c.CurrentUser = user
	}
	c.LayoutSections = make(map[string]string)
	vars.CurrentSite = c.LoadCurrentSite();

	vars.DomainsAllow = vars.GetDomainAllow()
	c.Data["BodyClass"] = &c.BodyClass
}

func (c *BaseController) CheckLogin() (*models.User, bool){
	data, success := c.GetSecureCookie(vars.AppKey, "userinfo")
	var userid uint
	if useridint, err := strconv.Atoi(data); success && err == nil{
		userid = uint(useridint)
	}else if c.GetSession("userinfo") != nil{
		userid = c.GetSession("userinfo").(uint)
	}
	u := &models.User{Id:userid}
	err := vars.Orm.Read(u)
	return u, err == nil
}


func (c *BaseController) DelLogin(){

}

func (c *BaseController) SetLogin(user *models.User, useCookie bool){
	if useCookie{
		c.SetSecureCookie(vars.AppKey, "userinfo", strconv.Itoa(int(user.Id)), 15552000)
	}else{
		c.SetSession("userinfo",user.Id)
	}
}

func (c *BaseController) SetTitle(Title string){
	if vars.CurrentSite.Name != ""{
		Title += " | " + vars.CurrentSite.Name
	}
	c.Data["Title"] = Title
}

func (c *BaseController) View(Name ...string) string{
	file := ""
	for i := 0 ; i < len(Name) ; i++{
		if(i != 0){
			file += "/"
		}
		file += Name[i]
	}
	return file + ".tpl"
}

func (c *BaseController) LoadCurrentSite() *vars.Site{
	site := &vars.Site{}
	var configs []*models.Config
	qs := vars.Orm.QueryTable(new(models.Config))
	qs.All(&configs)

	v := reflect.ValueOf(site).Elem()

	t := v.Type()

	for _, config := range configs{
		for i:= 0;i<t.NumField();i++{
			if t.Field(i).Name == config.Name{
				switch config.Type{
				case "time":
					t, err := time.Parse("2006-01-02 15:04:05 -0700 ICT", config.Value)
					if err == nil{
						v.Field(i).Set(reflect.ValueOf(t))
					}
				case "int":
					num, err := strconv.ParseInt(config.Value, 10, 64)
					if err != nil{
						num = 0
					}
					v.Field(i).Set(reflect.ValueOf(int(num)))
				default:
					v.Field(i).Set(reflect.ValueOf(config.Value))
				}
				continue
			}
		}
	}

	return site
}

func(c *BaseController) AddBodyClass(classes ...string){
	for _, class := range classes {
		c.BodyClass = append(c.BodyClass, class)
	}
}