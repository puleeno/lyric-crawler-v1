package controllers


type DashboardController struct{
	AdminController
}

func (c *DashboardController) Get(){
	c.SetTitle("Quản lý")
	c.TplName = c.View("dashboard", "index");
}