package controllers

import (
	"os"
	"errors"
	"github.com/astaxie/beego"
	"golang.org/x/crypto/bcrypt"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
	"gitlab.com/puleeno/lyric-crawler-v1/app/models"
)

type AuthController struct{
	BaseController
}

func (c *AuthController) Prepare(){
	c.BaseController.Prepare()
	view_path := vars.Cmd + "templates/" + vars.CurrentTheme + "/views/auth"
	if _, err := os.Stat(view_path); os.IsNotExist(err){
		view_path = vars.Cmd + "app/views/auth";
	}
	println(view_path)
	beego.SetViewsPath(view_path)
	c.Layout = "app.html"
}

func (c *AuthController) DoLogin(){

	if _, loggedin := c.CheckLogin(); loggedin{
		c.Redirect(c.URLFor("DashboardController.Get"), 302)
	}
	c.Data["Title"] = "Đăng nhập"
	Referer := c.GetString("url")
	if Referer == ""{
		Referer = c.Ctx.Input.Referer()
	}
	c.Data["Referer"] = Referer

	c.TplName = c.View("login")
	if(c.Ctx.Input.IsPost()){
		username := c.GetString("username")
		password := c.GetString("password")
		remember := c.GetString("remember") == "true"

		redirect := c.GetString("redirect")
		if redirect == ""{
			redirect = c.URLFor("DashboardController.Get")
		}

		user, err := c.Authenticate(username, password)

		if err == nil{
			c.SetLogin(user, remember)
			c.Redirect(redirect, 303)
		}
	}
}

func (c *AuthController) Authenticate(username string, password string) (user *models.User, err error){
	msg := "Tài khoản không tồn tại"
	user = &models.User{
		Username: username,
	}
	if err := vars.Orm.Read(user, "Username"); err != nil && err.Error() == "<QuerySeter> no row found"{
		user = &models.User{
			Email: username,
		}
		err = vars.Orm.Read(user, "Email")
	}

	if err != nil{
		err = errors.New(msg)
	}else if(user.Id < 0){
		err = errors.New(msg)
	}else if err := bcrypt.CompareHashAndPassword([]byte(user.Password),[]byte(password)); err != nil{
		return user, errors.New("Bạn vui lòng kiểm tra lại mật khẩu")
	}else{
		err = nil
	}
	return user, err
}