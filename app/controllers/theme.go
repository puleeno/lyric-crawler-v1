package controllers

import (
	"github.com/astaxie/beego"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
)

type ThemeController struct {
	BaseController
	CurrentPage string
}

func (c *ThemeController) Prepare(){
	c.BaseController.Prepare()
	beego.SetViewsPath(vars.Cmd+ "templates/" + vars.CurrentTheme + "/views")

	c.Data["BodyClass"] = &c.BodyClass
	c.Data["CurrentPage"] = &c.CurrentPage
	c.Data["CurrentSite"] = &vars.CurrentSite

	c.AddBodyClass("puleeno phone0981272899")
	c.Layout = "layout.html"
}