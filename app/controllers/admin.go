package controllers

import (
	"net/url"
	"github.com/astaxie/beego"
	"gitlab.com/puleeno/lyric-crawler-v1/app/vars"
)

type AdminController struct {
	BaseController
}

func(c *AdminController) Prepare(){
	c.BaseController.Prepare()
	if!c.IsLogin{
		val,_ := url.ParseQuery("url=" + c.Ctx.Input.URL())
		c.Redirect(vars.LoginURL + "?" + val.Encode(), 302)
	}
	beego.SetViewsPath(vars.Cmd + "app/views")

	c.LayoutSections["Navigation"] = c.View("partial", "navigation")
	c.Layout = "layout.html"

	c.AddBodyClass("admin")
	if c.LayoutSections["Sidebar"] != ""{
		c.AddBodyClass("sidebar")
	}
	c.Data["ActionMethod"] = "post"
	c.Data["AdminMenu"] = &vars.AdminMenu
}