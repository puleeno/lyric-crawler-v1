package controllers

type ErrorController struct{
	ThemeController
}

func(c *ErrorController) Error404(){
	c.TplName = c.View("error", "404")
}

func (c *ErrorController) Error403(){
	c.TplName = c.View("error" ,"403")
}