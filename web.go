package main

import (
	"os"
	"strconv"
	"github.com/astaxie/beego"
	_"gitlab.com/puleeno/lyric-crawler-v1/app"
	"gitlab.com/puleeno/lyric-crawler-v1/app/controllers"
	"gitlab.com/puleeno/lyric-crawler-v1/app/cron"
)

func init(){
	if(os.Getenv("OPENSHIFT_GO_IP") == ""){
		os.Setenv("OPENSHIFT_GO_IP", beego.AppConfig.String("httpaddr"))
	}
	if(os.Getenv("OPENSHIFT_GO_PORT") == ""){
		os.Setenv("OPENSHIFT_GO_PORT", beego.AppConfig.String("httpport"))
	}

	httpport,_ := strconv.Atoi(os.Getenv("OPENSHIFT_GO_PORT"))
	beego.BConfig.Listen.HTTPAddr = os.Getenv("OPENSHIFT_GO_IP")
	beego.BConfig.Listen.HTTPPort = httpport
	beego.BConfig.WebConfig.Session.SessionOn = true
}

func main() {
	task := os.Args[1:]
	fCron := false
	if(len(task) > 2 && task[0] == "cron"){
		cron.DetectCron(task)
		fCron = true
	}
	if(!fCron){
		beego.ErrorController(&controllers.ErrorController{})
		beego.Run()
	}
}